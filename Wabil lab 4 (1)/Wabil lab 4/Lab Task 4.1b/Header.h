#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;

class CSVRow // definging the members of a class row
{
	vector<string> m_data;
public:
	vector<float> rawMarks;
	string const& operator[](size_t index) const//overloading the operator []
	{
		return m_data[index];
	}
	size_t size() const
	{
		return m_data.size();
	}
	void readNextRow(std::istream& str) 
	{
		string line;
		getline(str, line);//default delimiter of the getline function is set as \n
		stringstream lineStream(line);//changing line's type stream to make it compatible with the getline function as first parameter of getline func has to be a stream
		string cell;
		m_data.clear();//destroying all elements stored in mdata
		while (std::getline(lineStream, cell, ','))// delimiter is now custom and a ','
		{
			m_data.push_back(cell);
		}
	}
};
istream& operator >> (istream& str, CSVRow& data)
{
	data.readNextRow(str);
	return str;
}