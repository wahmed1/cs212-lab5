#include "Header.h"

//Syed Wabil Ahmed
int main()
{
	ifstream file("sample-result-sheet.csv");//opens and preps the file.
	CSVRow row; // an object of class row
	while (file >> row)
	{
		int i = 2;
		float a;
		row.rawMarks.clear();

		while (i < 13)
		{

			a = stof(row[i]); //converts string to float for calculations.
			cout << row[i]<<endl;
			row.rawMarks.push_back(a);// adds a as an element to the vector rawMarks.
			i += 2;// increments the value of i by 2;
		}
		float a = row.rawMarks[1];
		float Assignment1 =  a* 0.05;
		a = row.rawMarks[2];
		float Assignment2 = a * 0.1;
		a = row.rawMarks[3];
		float Assignment3 = a * 0.15;
		a = row.rawMarks[4];
		float Midterm =a * 0.3;
		a = row.rawMarks[5];
		float Ese = a * 0.4;
		float Total = Assignment1 + Assignment2 + Assignment3 + Midterm + Ese;
		
		if (Total >= 80)
		{
			cout << "Name:\t " << row[0] << " " << row[1] << endl  << "Final:\t" << Total << "%" << endl << "Grade:\tH" << endl << endl;
		}
		if (Total >= 50 && Total < 80)
		{
			cout << "Name:\t" << row[0] << " " << row[1] << endl  << "Final:\t" << Total << "%" << endl << "Grade:\tR" << endl << endl;

		}
		if (Total < 50)
		{
			cout << "Name:\t" << row[0] << " " << row[1] << endl << "Final:\t" << Total << "%" << endl << "Grade:\tF" << endl << endl;

		}
		
	}
	cout << endl;
	cin.get();
	return 0;
}